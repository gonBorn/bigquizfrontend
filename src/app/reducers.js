import { combineReducers } from 'redux';
import goodsReducer from "./reducer/GoodsReducer";
import orderReducer from "./reducer/OrderReducer";


export default combineReducers({
    goodsReducer,
    orderReducer
});