import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {createGoods} from "../action/GoodsAction";


import './styles/createGoods.less'

class CreateGoods extends React.Component {

    constructor(props) {
        super(props);
        this.goods = {
            name: '',
            price: '',
            unit: '',
            url: ''
        };
        this.state = {
            disable: true
        };
        // this.inputName = this.inputName.bind(this,'name');
        // this.inputPrice = this.inputPrice.bind(this,'price');
        // this.inputUnit = this.inputUnit.bind(this,'unit');
        // this.inputUrl = this.inputUrl.bind(this,'url');
        // this.inputName = this.handleInput.bind(this,'name');
        // this.inputPrice = this.handleInput.bind(this,'price');
        // this.inputUnit = this.handleInput.bind(this,'unit');
        // this.inputUrl = this.handleInput.bind(this,'url');
        this.showSuccessMsg = this.showSuccessMsg.bind(this);
        this.showErrorMsg = this.showErrorMsg.bind(this);
        this.checkSubmitState = this.checkSubmitState.bind(this);
    }

    handleInput(type, e) {
        this.goods[type] = e.target.value
        this.checkSubmitState();
    }


    addGoods(e) {
        e.preventDefault();
        if (this.goods.name === "" || this.goods.price === "" || this.goods.unit === "" || this.goods.url === "") {
            alert("请填写完整的信息")
        } else {
            this.props.createGoods(this.goods, this.showSuccessMsg, this.showErrorMsg);
        }
    }

    showSuccessMsg() {
        window.location.href = "/mall/goods";
    }

    showErrorMsg() {
        alert("商品信息有误");
    }

    checkSubmitState() {
        if (this.goods.name === "" || this.goods.price === "" || this.goods.unit === "" || this.goods.url === "" || isNaN(this.goods.price)) {
            this.setState(
                {
                    disable: true
                }
            );
        } else {
            this.setState(
                {
                    disable: false
                }
            );
        }
    }

    render() {
        return (
            <div className='page'>
                <form className='page-core'>
                    <h1>添加商品</h1>
                    <section>
                        <label><span>*</span>名称:</label>
                        <input type="text" placeholder={"名称"} id='name' onChange={this.handleInput.bind(this, 'name')}/>
                    </section>
                    <section>
                        <label><span>*</span>价格:</label>
                        <input type="text" placeholder={"价格"} id='price'
                               onChange={this.handleInput.bind(this, 'price')}/>

                    </section>
                    <section>
                        <label><span>*</span>单位:</label>
                        <input type="text" placeholder={"单位"} id='unit' onChange={this.handleInput.bind(this, 'unit')}/>
                    </section>
                    <section>
                        <label><span>*</span>图片:</label>
                        <input type="text" placeholder={"url"} id='url' onChange={this.handleInput.bind(this, 'url')}/>
                    </section>
                    <section>
                        <button onClick={this.addGoods.bind(this)} disabled={this.state.disable ? "disabled" : ""}>提交</button>
                    </section>
                </form>
            </div>
        )
    }

}

const mapStateToProps = state => ({
    goodsList: state.goodsReducer.goods
});

const mapDispatchToProps = dispatch => bindActionCreators({
    createGoods
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CreateGoods);