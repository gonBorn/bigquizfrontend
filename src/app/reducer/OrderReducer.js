const initState = {
    orders:[]
};

const orderReducer = (state = initState, action) => {
    switch (action.type) {
        case 'GET_ORDER_LIST':
            return {
                ...state,
                orders: action.orders
            };
        default:
            return state
    }
};

export default orderReducer;