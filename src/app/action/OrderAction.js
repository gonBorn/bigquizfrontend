const getOrderList = () => (dispatch) => {
    fetch("http://localhost:8080/mall/orders")
        .then(response => response.json().then(data => {
                dispatch({
                    type: "GET_ORDER_LIST",
                    orders: data
                })
            })
        );

};

const deleteOrderById = (id) => (dispatch) => {
    fetch("http://localhost:8080/mall/orders/delete/" + id, {
        method: 'DELETE',
    }).then(
        response=>{
            window.location.reload();
            getOrderList(dispatch);
            dispatch({
                type: "DELETE_CART",
                payload: {
                    deleted: true
                }
            })

        }
    );
};

const updateOrder = (id) => (dispatch) => {
    fetch("http://localhost:8080/mall/orders/update/" + id, {
        method: "PATCH",
    }).then(
        response=>{

        }
    );
};

export {
    getOrderList,
    deleteOrderById,
    updateOrder
}