const getGoodsList = () => (dispatch) => {
    fetch("http://localhost:8080/mall/goods")
        .then(response => response.json())
            .then(data => {
                dispatch({
                    type: "GET_GOODS_LIST",
                    goods: data
                })
            });
};

const createGoods = (data, successCallback, errorCallBack) => (dispatch) => {
    fetch("http://localhost:8080/mall/goods/create",{
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body:JSON.stringify(data)
    }).then(successCallback).catch(errorCallBack);
}

export {
    getGoodsList,
    createGoods
}