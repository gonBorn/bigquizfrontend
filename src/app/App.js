import React from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import Header from "./Components/Header";
import Home from "./Pages/Home";
import {Route, Switch} from "react-router";
import CreateGoods from "./Pages/CreateGoods";
import Order from "./Pages/Order";

import './App.less';

const App = () => {
  return (
    <div className="App">
      <Router>
          <Header/>
        <Switch>
          <Route path="/mall/orders" component={Order} />
          <Route path="/mall/goods/create" component={CreateGoods} />
          <Route path="/mall/goods" component={Home} />
      </Switch>
      </Router>
    </div>
  );
};

export default App;