import React from "react";
import './styles/header.less'
import {Link, NavLink} from "react-router-dom";
import { MdHome } from "react-icons/md";
import { MdShoppingCart } from "react-icons/md";
import { MdLibraryAdd } from "react-icons/md";
export default class Header extends React.Component{
    render() {
        return(
            <header className='background'>
                <NavLink exact to={"/mall/goods"} activeClassName='selected'><div className='tab'><MdHome />  商城</div></NavLink>
                <NavLink to={"/mall/orders"} activeClassName='selected'><div className='tab'><MdShoppingCart />  订单</div></NavLink>
                <NavLink to={"/mall/goods/create"} activeClassName='selected'><div className='tab'><MdLibraryAdd />  添加商品</div></NavLink>
            </header>
        );
    }
}