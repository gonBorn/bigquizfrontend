import React from 'react';
import {MdAddCircleOutline} from "react-icons/md";
import {getGoodsList} from "../action/GoodsAction";
import {connect} from "react-redux";
import {Home} from "../Pages/Home";
import {updateOrder} from "../action/OrderAction";
import {bindActionCreators} from "redux";
import './styles/goodsTab.less';


class GoodsTab extends React.Component {

    constructor(props) {
        super(props);
    }


    addAGoodsToOrder(id) {
        this.props.updateOrder(id);
    }

    render() {
        return (
            <div className='tabBorder'>
                <section>
                    <img src={this.props.url}/>
                    <div className='goodsName'>{this.props.name}</div>
                    <div className='price'>
                        单价：{this.props.price}元/{this.props.unit}
                    </div>
                    <button onClick={this.addAGoodsToOrder.bind(this, this.props.id)}><MdAddCircleOutline/></button>
                </section>
            </div>
        )
    }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => bindActionCreators({
    updateOrder
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(GoodsTab);