const initState = {
    goods:[]
};

const goodsReducer = (state = initState, action) => {
    switch (action.type) {
        case 'GET_GOODS_LIST':
            return {
                ...state,
                goods: action.goods
            };
        default:
            return state
    }
};

export default goodsReducer;