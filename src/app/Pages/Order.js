import React, {Fragment} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {deleteOrderById, getOrderList} from "../action/OrderAction";
import {Link} from "react-router-dom";
import './styles/order.less';
import '../App.less';

class Order extends React.Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        this.props.getOrderList();
    }

    deleteOrder(id) {
        this.props.deleteOrderById(id);
    }

    render() {
        return (
            <section className='page'>
            {
                this.props.orderList.length == 0 ? <p>暂无订单，<Link to='/mall/goods'>返回商城</Link>继续购买</p> :
                    <table className='page-core'>
                    <tbody>
                    <tr className={"table-header"}>
                        <th>名字</th>
                        <th>单价</th>
                        <th>数量</th>
                        <th>单位</th>
                        <th>操作</th>
                    </tr>
                    {
                        (this.props.orderList || []).map((order, index) => {
                            return (
                                <tr key={order.id} className={"table-tr"}>
                                    <td>{order.goods.name}</td>
                                    <td>{order.goods.price}</td>
                                    <td>{order.num}</td>
                                    <td>{order.goods.unit}</td>
                                    <td>
                                        <button onClick={this.deleteOrder.bind(this, order.id)}>删除</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>
            }
            </section>

        )
    }
}

const mapStateToProps = state => ({
    orderList: state.orderReducer.orders
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getOrderList,
    deleteOrderById
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Order);