import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getGoodsList} from "../action/GoodsAction";
import GoodsTab from "../Components/GoodsTab";

import './styles/home.less';

class Home extends React.Component {

    componentDidMount() {
        this.props.getGoodsList();
    }

    render() {
        console.log(this.props.goodsList);
        return (

            <section className='page'>
                <div className='page-core'>
                    {
                        (this.props.goodsList || []).map((goods, index) => {
                            console.log(goods);
                            return (
                                <GoodsTab key={index} url={goods.url} id={goods.id} name={goods.name}
                                          price={goods.price} unit={goods.unit}/>
                            )
                        })
                    }
                </div>
            </section>
        )
    }

}

const mapStateToProps = state => ({
    goodsList: state.goodsReducer.goods
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getGoodsList
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);